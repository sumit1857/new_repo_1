package com.collabera.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.collabera.dao.UsersDao;
import com.collabera.model.Users;

/**
 * Servlet implementation class FetchController
 */
public class FetchController extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("id"));
		
		UsersDao dao = new UsersDao();		
		Users users = dao.getUsers(id);
		
		request.setAttribute("users", users);
		
		RequestDispatcher rd = request.getRequestDispatcher("records.jsp");
		rd.forward(request, response);
		
	}

	

}
